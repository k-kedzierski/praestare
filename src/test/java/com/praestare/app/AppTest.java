package com.praestare.app;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import com.praestare.exception.WrongIdException;
import com.praestare.exception.WrongRelationException;
import com.praestare.issue.epic.entity.Epic;
import com.praestare.issue.task.entity.Task;
import com.praestare.service.DatabaseService;
import com.praestare.service.EntityService;
import com.praestare.utils.Priority;
import com.praestare.utils.Status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class AppTest {

    @Mock
    private DatabaseService databaseService;

    private Epic MockEpic(int id) {
        Epic epic = Mockito.mock(Epic.class);
        Mockito.lenient().when(epic.getId()).thenReturn(id);

        epic.setName("Epic 1");
        epic.setDescription("Epic 1 description");

        return epic;
    }

    private Task MockTask(Epic epic) {
        Task task = Mockito.mock(Task.class);

        task.setName("Task 1");
        task.setDescription("Task 1 description");
        task.setStatus(Status.TODO);
        task.setPriority(Priority.HIGH);
        task.setEpic(epic);

        return task;
    }

    private static Stream<Arguments> argumentProvider() {
        return Stream.of(
                Arguments.of("epic", -1, "Id has to be a positive number.", WrongIdException.class),
                Arguments.of("", 1, "Relation has to be a valid string.", WrongRelationException.class));
    }

    @Test
    public void SaveEpicToDatabase_PersistedOnlyOnce() {
        // Arrange
        EntityService<Epic> epicService = new EntityService<Epic>(databaseService, Epic.class);
        int epicId = 1;
        Epic epic = MockEpic(epicId);

        // ACT
        epicService.save(epic);

        // ASSERT
        Mockito.verify(databaseService, Mockito.times(1)).save(epic);
    }

    @Test
    public void FetchAllRelatedTasks() throws Exception {
        // ARRANGE
        EntityService<Task> taskService = new EntityService<Task>(databaseService, Task.class);

        int epicId = 1;
        Epic epic = MockEpic(epicId);
        Task task = MockTask(epic);
        List<Task> tasks = Collections.singletonList(task);

        // Return list of task
        Mockito.when(databaseService.makeRelationQuery("epic", epicId, Task.class)).thenReturn(tasks);

        // ACT
        List<Task> result = taskService.getByRelation("epic", epic.getId());

        // ASSERT
        assertThat(result).isEqualTo(tasks);

    }

    @Test
    public void FindTaskByIdValidation() {
        // ARRANGE
        EntityService<Task> taskService = new EntityService<Task>(databaseService, Task.class);
        String expectedMessage = "Id has to be a positive number.";

        // ACT & ASSERT
        assertThatExceptionOfType(WrongIdException.class).isThrownBy(() -> taskService.findById(-1))
                .withMessage(expectedMessage);
    }

    @ParameterizedTest()
    @MethodSource("argumentProvider")
    public void FindTaskByRelationValidation(String relation, int id, String expectedMessage,
            Class<? extends Exception> exception) {
        // ARRANGE
        EntityService<Task> taskService = new EntityService<Task>(databaseService, Task.class);

        // ACT & ARRANGE
        assertThatExceptionOfType(exception).isThrownBy(() -> taskService.getByRelation(relation, id))
                .withMessage(expectedMessage);
    }

    @Test
    public void DeleteTask_DeletedOnlyOnce() {
        // ARRANGE
        EntityService<Task> taskService = new EntityService<Task>(databaseService, Task.class);
        Task task = MockTask(MockEpic(1));

        // ACT
        taskService.delete(task);

        // ASSERT
        Mockito.verify(databaseService, Mockito.times(1)).delete(task);
    }

    @Test
    public void UpdateTask_UpdatedOnlyOnce() {
        // ARRANGE
        EntityService<Task> taskService = new EntityService<Task>(databaseService, Task.class);
        Task task = MockTask(MockEpic(1));

        // ACT
        taskService.update(task);

        // ASSERT
        Mockito.verify(databaseService, Mockito.times(1)).update(task);
    }

    @Test
    public void Task_ProperToStringImplementation() {
        // ARRANGE
        Task task = new Task();
        task.setName("Task 1");
        task.setDescription("Task 1 description");
        task.setStatus(Status.TODO);
        task.setPriority(Priority.HIGH);

        // ACT
        String result = task.toString();

        // ASSERT
        assertThat(result).isEqualTo("[task] :: 0 :: Task 1");
    }

    @Test
    public void Epic_ProperToStringImplementation() {
        // ARRANGE
        Epic epic = new Epic();
        epic.setName("Epic 1");
        epic.setDescription("Epic 1 description");

        // ACT
        String result = epic.toString();

        // ASSERT
        assertThat(result).isEqualTo("[epic] :: 0 :: Epic 1");
    }

    @Test
    public void Task_ProperEqualsImplementation() {
        // ARRANGE
        Task task1 = new Task();
        task1.setName("Task 1");
        task1.setDescription("Task 1 description");
        task1.setStatus(Status.TODO);
        task1.setPriority(Priority.HIGH);

        Task task2 = new Task();
        task2.setName("Task 1");
        task2.setDescription("Task 1 description");
        task2.setStatus(Status.TODO);
        task2.setPriority(Priority.HIGH);

        // Since entity manager is mocked, the @GeneratedValue annotation is not used
        // and the id is set to 0. In the production environment, the id is generated
        // by the database. Therefore, the id for task 2 is set to 1 manually here,
        // to reflect the production environment.
        task2.setId(1);

        // ACT
        boolean result = task1.equals(task2);

        // ASSERT
        assertThat(result).isFalse();
    }

    @Test
    public void Epic_CheckIfTaskSetIsEmpty() {
        // ARRANGE
        Epic epic = new Epic();

        // ACT
        boolean result = epic.getTasks().isEmpty();

        // ASSERT
        assertThat(result).isTrue();
    }

    @Test
    public void Epic_CheckIfTaskSetIsNotEmpty() {
        // ARRANGE
        Epic epic = new Epic();
        Task task = new Task();
        epic.getTasks().add(task);

        // ACT
        boolean result = epic.getTasks().isEmpty();

        // ASSERT
        assertThat(result).isFalse();
    }
}
