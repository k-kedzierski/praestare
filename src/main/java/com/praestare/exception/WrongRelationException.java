package com.praestare.exception;

public class WrongRelationException extends Exception {

    public WrongRelationException() {
        super("Relation has to be a valid string.");
    }

}