package com.praestare.exception;

public class WrongIdException extends Exception {

    public WrongIdException() {
        super("Id has to be a positive number.");
    }

}