package com.praestare.service;

import java.util.List;

import com.praestare.exception.WrongIdException;
import com.praestare.exception.WrongRelationException;

public class EntityService<T> {
    public final DatabaseService databaseService;
    private final Class<T> clazz;

    /** CONTAINER-MANAGED APPROACH! */
    public EntityService(DatabaseService databaseService, Class<T> clazz) {
        this.databaseService = databaseService;
        this.clazz = clazz;
    }
    
    public T findById(int id) throws WrongIdException {
        validateId(id);

        return databaseService.find(clazz, id);
    }

    public List<T> findAll() {
        return databaseService.findAll(clazz);
    }

    public List<T> getByRelation(String relation, int id) throws Exception {
        validateRelation(relation);
        validateId(id);

        return makeRelationQuery(relation, id);
    }

    public void save(T entity) {
        databaseService.save(entity);
    }

    public void update(T entity) {
        databaseService.update(entity);
    }

    public void delete(T entity) {
        databaseService.delete(entity);
    }

    public List<T> makeRelationQuery(String relation, int id) {
        return databaseService.makeRelationQuery(relation, id, clazz);
    }

    private void validateId(int id) throws WrongIdException {
        if (id < 0) {
            throw new WrongIdException();
        }
    }
    
    private void validateRelation(String relation) throws WrongRelationException {
        if (relation.isEmpty()) {
            throw new WrongRelationException();
        }
    }
}