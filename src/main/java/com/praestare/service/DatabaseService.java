package com.praestare.service;

import javax.persistence.EntityManager;
import java.util.List;

public class DatabaseService {

    private final EntityManager entityManager;

    public DatabaseService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void beginTransaction() {
        entityManager.getTransaction().begin();
    }

    public void commitTransaction() {
        entityManager.getTransaction().commit();
    }

    public <T> List<T> makeRelationQuery(String relation, int id, Class<T> clazz) {
        return entityManager.createQuery(
                "SELECT t FROM " + clazz.getSimpleName() + " t WHERE t." + relation + ".id = " + id, clazz
        ).getResultList();
    }

    public <T> T find(Class<T> clazz, int id) {
        return entityManager.find(clazz, id);
    }

    public <T> List<T> findAll(Class<T> clazz) {
        return entityManager.createQuery("SELECT t FROM " + clazz.getSimpleName() + " t", clazz).getResultList();
    }

    public <T> void save(T entity) {
        beginTransaction();
        entityManager.persist(entity);
        commitTransaction();
    }

    public <T> void update(T entity) {
        beginTransaction();
        entityManager.merge(entity);
        commitTransaction();
    }

    public <T> void delete(T entity) {
        beginTransaction();
        entityManager.remove(entity);
        commitTransaction();
    }

}
