package com.praestare.tag.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.*;

import lombok.*;

@Entity
@Table(name = "tag")
@Getter
@Setter
@EqualsAndHashCode()
@NoArgsConstructor
@AllArgsConstructor
public class Tag implements Serializable {
    
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        private int id;
    
        @Column(name = "name")
        private String name;
    
        @Column(name = "description")
        private String description;
    
        @Column(name = "create_date")
        private LocalDateTime createDate;
    
        @Override
        public String toString() {
            return "[tag] :: " + this.id + " :: " + this.name;
        }

        public String displayString() {
            String line = new String(new char[this.toString().length()]).replace("\0", "─");

            String renderedIssue = "┌─" + line + "─┐" + "\n" +
                    "│ " + this.toString() + " │" + "\n" +
                    "└─" + line + "─┘" + "\n" +
                    "\n" +
                    "Description: " + this.description + "\n" +
                    "Create Date: " + this.createDate + "\n";

            return renderedIssue;
        }

    
}
