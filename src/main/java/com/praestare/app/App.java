package com.praestare.app;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.praestare.service.DatabaseService;
import com.praestare.utils.Priority;
import com.praestare.utils.Status;

import com.praestare.issue.epic.entity.Epic;
import com.praestare.issue.task.entity.Task;
import com.praestare.service.EntityService;
import com.praestare.tag.entity.Tag;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * <h1>Praestare (lat. "Perform") app</h1>
 * 
 * Task management software.
 *
 * @throws Exception
 * @since 2022-03-22
 */
public class App {
    private static Logger logger = LogManager.getLogger(App.class);
    
    private static final EntityManagerFactory entityManagerFactory = Persistence
            .createEntityManagerFactory("com.praestare.app.praestare-app.1.0-SNAPSHOT");
    private static final EntityManager entityManager = entityManagerFactory.createEntityManager();
    private static final DatabaseService databaseService = new DatabaseService(entityManager);

    public static void main(String[] args) throws Exception {
        System.out.println("Praestaree app.\n");

        EntityService<Task> taskService = new EntityService<Task>(databaseService, Task.class);
        EntityService<Epic> epicService = new EntityService<Epic>(databaseService, Epic.class);
        EntityService<Tag> tagService = new EntityService<Tag>(databaseService, Tag.class);

        try (Scanner scanner = new Scanner(System.in).useDelimiter("\r?\n")) {
            while (true) {
                System.out.print(">>> ");
                String cmd = scanner.next();

                switch (cmd) {
                    case "create task":
                        System.out.print("... name :: ");
                        String name = scanner.next();

                        System.out.print("... description :: ");
                        String description = scanner.next();

                        System.out.print("... priority :: ");
                        String priorityString = scanner.next();
                        Priority priority = Priority.valueOf(priorityString);

                        System.out.print("... status :: ");
                        String statusString = scanner.next();
                        Status status = Status.valueOf(statusString);

                        System.out.print("... epic :: ");
                        String epicString = scanner.next();

                        try {
                            Epic epic = epicService.findById(Integer.valueOf(epicString));
                            Task task = new Task();
                            task.setName(name);
                            task.setDescription(description);
                            task.setPriority(priority);
                            task.setStatus(status);
                            task.setEpic(epic);
                            taskService.save(task);
                        } catch (Exception e) {
                            System.out.println("Epic not found.");
                        }

                        break;
                    case "list task":
                        List<Task> tasks = taskService.findAll();
                        Collections.sort(tasks);
                        for (Task task : tasks) {
                            System.out.println(task);
                        }
                        break;
                    case "list task --detailed":
                        List<Task> tasksDetailed = taskService.findAll();
                        Collections.sort(tasksDetailed);
                        for (Task task : tasksDetailed) {
                            System.out.println(task.displayString());
                        }
                        break;
                    case "change task status":
                        System.out.print("... id :: ");
                        int id = scanner.nextInt();

                        System.out.print("... status :: ");
                        String statusString2 = scanner.next();
                        Status status2 = Status.valueOf(statusString2);

                        try {
                            // Get Task entity, change status and update it
                            Task task = taskService.findById(id);
                            task.setStatus(status2);
                            taskService.update(task);
                        } catch (Exception e) {
                            System.out.println("Task not found.");
                        }

                        break;
                    case "remove task":
                        System.out.print("... id :: ");
                        int idToRemove = scanner.nextInt();

                        try {
                            Task taskToRemove = taskService.findById(idToRemove);
                            taskService.delete(taskToRemove);
                        } catch (Exception e) {
                            System.out.println("Task not found.");
                        }
                        break;
                    case "create epic":
                        System.out.print("... name :: ");
                        String nameEpic = scanner.next();

                        System.out.print("... description :: ");
                        String descriptionEpic = scanner.next();

                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

                        System.out.print("... due date (yyyy-mm-dd) :: ");
                        String dueDateString = scanner.next();

                        try {
                            LocalDate dueDate = LocalDate.parse(dueDateString, formatter);
                            Epic epic = new Epic();
                            epic.setName(nameEpic);
                            epic.setDescription(descriptionEpic);
                            epic.setDueDate(dueDate);
                            epicService.save(epic);
                        } catch (Exception e) {
                            System.out.println("Invalid date format");
                            continue;
                        }
                        break;
                    case "list epic":
                        List<Epic> epics = epicService.findAll();
                        Collections.sort(epics);
                        for (Epic epic : epics) {
                            System.out.println(epic);
                        }
                        break;
                    case "list epic --detailed":
                        List<Epic> epicsDetailed = epicService.findAll();
                        Collections.sort(epicsDetailed);
                        for (Epic epic : epicsDetailed) {
                            System.out.println(epic.displayString());
                            System.out.println("\n Tasks:");
                            for (Task task : taskService.getByRelation("epic", epic.getId())) {
                                System.out.println(task);
                            }
                        }
                        break;
                    case "modify epic due date":
                        System.out.print("... id :: ");
                        int idEpic = scanner.nextInt();

                        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");

                        System.out.print("... due date (yyyy-mm-dd) :: ");
                        String dueDateString2 = scanner.next();

                        try {
                            LocalDate dueDate = LocalDate.parse(dueDateString2, formatter2);
                            Epic epic = epicService.findById(idEpic);
                            epic.setDueDate(dueDate);
                            epicService.update(epic);
                        } catch (Exception e) {
                            System.out.println("Invalid date format or id not found.");
                            continue;
                        }
                        break;
                    case "remove epic":
                        System.out.print("... id :: ");
                        int idEpicToRemove = scanner.nextInt();

                        try {
                            for (Task task : taskService.getByRelation("epic", idEpicToRemove)) {
                                taskService.delete(task);
                            }
                            Epic epicToRemove = epicService.findById(idEpicToRemove);
                            epicService.delete(epicToRemove);
                        } catch (Exception e) {
                            System.out.println("Epic not found.");
                        }
                        break;
                    case "create tag":
                        System.out.print("... name :: ");
                        String nameTag = scanner.next();

                        System.out.print("... description :: ");
                        String descriptionTag = scanner.next();

                        Tag newTag = new Tag();
                        newTag.setName(nameTag);
                        newTag.setDescription(descriptionTag);
                        newTag.setCreateDate(LocalDateTime.now());
                        tagService.save(newTag);
                        break;
                    case "list tag":
                        List<Tag> tags = tagService.findAll();
                        for (Tag tag : tags) {
                            System.out.println(tag);
                        }
                        break;
                    case "list tag --detailed":
                        List<Tag> tagsDetailed = tagService.findAll();
                        for (Tag tag : tagsDetailed) {
                            System.out.println(tag.displayString());
                        }
                        break;
                    case "remove tag":
                        System.out.print("... id :: ");
                        int idTagToRemove = scanner.nextInt();

                        try {
                            Tag tagToRemove = tagService.findById(idTagToRemove);
                            tagService.delete(tagToRemove);
                        } catch (Exception e) {
                            System.out.println("Tag not found.");
                        }
                        break;
                    case "help":
                        System.out.print("<create|list|list remove> <task|epic|tag>, exit\n");
                        break;
                    case "q":
                    case "quit":
                    case "exit":
                        return;
                    default:
                        System.out.println("cannot resolve command :: " + cmd);
                }
            }
        }
    }
}
