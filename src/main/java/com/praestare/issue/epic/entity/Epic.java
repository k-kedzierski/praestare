package com.praestare.issue.epic.entity;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import com.praestare.issue.Issue;
import com.praestare.issue.task.entity.Task;

import lombok.*;
import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "epic")
public class Epic extends Issue implements Comparable<Epic>, Cloneable {

    @Column(name = "due_date")
    protected LocalDate dueDate;

    @OneToMany(mappedBy = "epic", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Task> tasks = new HashSet();

    // Polymorphism example - method override
    @Override
    public String displayString() {
        String line = new String(new char[this.toString().length()]).replace("\0", "─");

        String renderedIssue = "┌─" + line + "─┐" + "\n" +
                "│ " + this.toString() + " │" + "\n" +
                "└─" + line + "─┘" + "\n" +
                "\n" +
                "Description: " + this.description + "\n" +
                "Due Date: " + this.dueDate + "\n";

        return renderedIssue;
    }

    @Override
    public String toString() {
        return "[epic] :: " + super.toString();
    }

    // Implementing Comparable interface
    @Override
    public int compareTo(Epic o) {
        return this.dueDate.compareTo(o.dueDate);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Epic cloned = (Epic)super.clone();
        // Since java.util.Date is neither primitive not immutable, is deep
        // copy required here?
        cloned.dueDate = this.dueDate.plusDays(1);
        return cloned;
    }

}
