package com.praestare.issue;

import java.io.Serializable;

import javax.persistence.*;

import lombok.*;

/**
 * Abstract {@code Issue} class.
 * <p>
 * Issue serves as a core generic component in task management tree for
 * inheriting concrete implementations, such as {@code Task}, or {@code Epic}.
 */

@Getter
@Setter
@EqualsAndHashCode()
@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
public abstract class Issue implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected int id;

    @Column(name = "name")
    protected String name;

    @Column(name = "description")
    protected String description;

    /**
     * Display String-formatted issue with exhaustive information of the 
     * component.
     * 
     * @return String-formatted detailed representation of the issue
     */
    public abstract String displayString();

    /**
     * @return String representation of the issue
     */
    // Polymorphism example - method override
    @Override
    public String toString() {
        return this.id + " :: " + this.name;
    }
    
}