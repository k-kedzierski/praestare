package com.praestare.issue.task.entity;

import com.praestare.issue.Issue;
import com.praestare.issue.epic.entity.Epic;
import com.praestare.utils.Priority;
import com.praestare.utils.Status;

import lombok.*;
import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "task")
@NoArgsConstructor
@AllArgsConstructor
public class Task extends Issue implements Comparable<Task> {

    @Column(name = "priority")
    @Enumerated(EnumType.STRING)
    private Priority priority;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "epic_id")
    private Epic epic;

    // Polymorphism example - method override
    @Override
    public String displayString() {
        String line = new String(new char[this.toString().length()]).replace("\0", "─");

        String renderedIssue = "┌─" + line + "─┐" + "\n" +
                "│ " + this.toString() + " │" + "\n" +
                "└─" + line + "─┘" + "\n" +
                "\n" +
                "Description: " + this.description + "\n" +
                "Priority: " + this.priority + "\n" +
                "Status: " + this.status + "\n";

        return renderedIssue;
    }
    
    @Override
    public String toString() {
        return "[task] :: " + super.toString();
    }

    @Override
    public int compareTo(Task o) {
        if (this.id < o.id) {
            return -1;
        } else if (this.id > o.id) {
            return 1;
        } else {
            return 0;
        }
    }
}
