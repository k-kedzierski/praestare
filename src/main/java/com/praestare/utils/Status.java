package com.praestare.utils;

public enum Status {
    POSTPONED,
    TODO,
    IN_PROGRESS,
    TO_REVIEW,
    IN_REVIEW,
    DONE
}
