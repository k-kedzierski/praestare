package com.praestare.utils;

public enum Priority {
    VERY_LOW,
    LOW,
    MEDIUM,
    HIGH,
    VERY_HIGH;

    // Multithreading-optimized random Priority generator
    public static Priority random() {
        return values()[(int) (Math.random() * values().length)];
    }
}
