# praestare

Task management software.

Build the project:
```sh
mvn package
```

And run the compiled and packaged .jar:
```sh
java -cp target/preastare-app-1.0-SNAPSHOT.jar com.preastare.app.App
```